package br.edu.up;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.ResourceBundle;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.stage.Stage;

public class ClienteVotacaoFx extends Application implements Initializable {

  private Urna urna;
  private Cedula cedula;
  private Candidato candidato;

  @FXML
  private Button btnSair;
  
  @FXML
  private Button btnZero;

  @FXML
  private Button btnUm;

  @FXML
  private Button btnDois;

  @FXML
  private Button btnTres;

  @FXML
  private Button btnQuatro;

  @FXML
  private Button btnCinco;

  @FXML
  private Button btnSeis;

  @FXML
  private Button btnSete;

  @FXML
  private Button btnOito;

  @FXML
  private Button btnNove;

  @FXML
  private Button btnBranco;

  @FXML
  private Button btnCorrigir;

  @FXML
  private Button btnConfirmar;

  @FXML
  private ImageView imgCandidato;

  @FXML
  private Label lblNome;

  @FXML
  private TextField txtDigito1;

  @FXML
  private TextField txtDigito2;

  @FXML
  private TextField txtTitulo;

  @FXML
  private Button btnVotar;

  @FXML
  private Label lblInvalido;

  private void definirNumero(String numero) {
    if ("".equals(txtDigito1.getText())) {
      txtDigito1.setText(numero);
    } else if ("".equals(txtDigito2.getText())) {
      txtDigito2.setText(numero);
    }

    if (!"".equals(txtDigito1.getText()) && !"".equals(txtDigito2.getText()) && imgCandidato.getImage() == null) {

      String codigo = txtDigito1.getText() + txtDigito2.getText();
      try {
        candidato = urna.getCandidato(codigo);
        if (candidato == null) {
          candidato = urna.getCandidato("x");
        }
        InputStream bais = new ByteArrayInputStream(candidato.getFoto());
        Image foto = new Image(bais);
        imgCandidato.setImage(foto);
        lblNome.setText(candidato.getNome());
      } catch (RemoteException e) {
        e.printStackTrace();
      }

    }
  }

  @Override
  public void initialize(URL location, ResourceBundle resources) {

    try {
      urna = (Urna) Naming.lookup("rmi://localhost:9090/urna");
    } catch (MalformedURLException e) {
      e.printStackTrace();
    } catch (RemoteException e) {
      Alert alert = new Alert(AlertType.ERROR);
      alert.setTitle("Aten��o!");
      alert.setHeaderText("N�o foi poss�vel conectar ao servidor!");
      alert.setContentText("Certifique-se de que ele j� foi iniciado!");
      alert.showAndWait();
      Platform.exit();
    } catch (NotBoundException e) {
      e.printStackTrace();
    }

    if (btnZero != null) {
      btnZero.setOnAction(new EventHandler<ActionEvent>() {
        @Override
        public void handle(ActionEvent event) {
          definirNumero("0");
        }
      });
    }

    if (btnUm != null) {
      btnUm.setOnAction(new EventHandler<ActionEvent>() {
        @Override
        public void handle(ActionEvent event) {
          definirNumero("1");
        }
      });
    }

    if (btnDois != null) {
      btnDois.setOnAction(new EventHandler<ActionEvent>() {
        @Override
        public void handle(ActionEvent event) {
          definirNumero("2");
        }
      });
    }

    if (btnTres != null) {
      btnTres.setOnAction(new EventHandler<ActionEvent>() {
        @Override
        public void handle(ActionEvent event) {
          definirNumero("3");
        }
      });
    }

    if (btnQuatro != null) {
      btnQuatro.setOnAction(new EventHandler<ActionEvent>() {
        @Override
        public void handle(ActionEvent event) {
          definirNumero("4");
        }
      });
    }

    if (btnCinco != null) {
      btnCinco.setOnAction(new EventHandler<ActionEvent>() {
        @Override
        public void handle(ActionEvent event) {
          definirNumero("5");
        }
      });
    }

    if (btnSeis != null) {
      btnSeis.setOnAction(new EventHandler<ActionEvent>() {
        @Override
        public void handle(ActionEvent event) {
          definirNumero("6");
        }
      });
    }

    if (btnSete != null) {
      btnSete.setOnAction(new EventHandler<ActionEvent>() {
        @Override
        public void handle(ActionEvent event) {
          definirNumero("7");
        }
      });
    }

    if (btnOito != null) {
      btnOito.setOnAction(new EventHandler<ActionEvent>() {
        @Override
        public void handle(ActionEvent event) {
          definirNumero("8");
        }
      });
    }

    if (btnNove != null) {
      btnNove.setOnAction(new EventHandler<ActionEvent>() {
        @Override
        public void handle(ActionEvent event) {
          definirNumero("9");
        }
      });
    }

    if (btnCorrigir != null) {
      btnCorrigir.setOnAction(new EventHandler<ActionEvent>() {
        @Override
        public void handle(ActionEvent event) {
          txtDigito1.setText("");
          txtDigito2.setText("");
          imgCandidato.setImage(null);
          lblNome.setText("");
          candidato = null;
        }
      });
    }
  }

  public static void main(String[] args) {
    Application.launch(args);
  }

  @FXML
  private void handleButtonAction(ActionEvent event) throws IOException {
    if (event.getSource() == btnVotar) {
      lblInvalido.setText("");
      try {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("TelaDeVotacao.fxml"));
        Cedula cedula = urna.getCedula(txtTitulo.getText());
        if (cedula != null) {
          Stage tela = (Stage) btnVotar.getScene().getWindow();
          Parent parent = loader.load();
          ClienteVotacaoFx cliente = loader.<ClienteVotacaoFx> getController();
          cliente.setCedula(cedula);
          Scene cenario = new Scene(parent);
          tela.setScene(cenario);
          tela.show();
        } else {
          lblInvalido.setText("O eleitor informado � inv�lido!");
        }
      } catch (RemoteException e) {
        e.printStackTrace();
      }
    } else if (event.getSource() == btnConfirmar && candidato != null) {
      cedula.setCandidato(candidato);
      urna.votar(cedula);
      
      File arquivo = new File("rsc/urna.mp3");
      Media media = new Media(arquivo.toURI().toString());
      MediaPlayer mp = new MediaPlayer(media);
      mp.play();
      
      Stage tela = (Stage) btnConfirmar.getScene().getWindow();
      Parent parent = FXMLLoader.load(getClass().getResource("TelaDeEleitor.fxml"));
      Scene cenario = new Scene(parent);
      tela.setScene(cenario);
      tela.show();
    } else if (event.getSource() == btnBranco) {
      Candidato candidato = urna.getCandidato("0");
      cedula.setCandidato(candidato);
      urna.votar(cedula);
      
      File arquivo = new File("rsc/urna.mp3");
      Media media = new Media(arquivo.toURI().toString());
      MediaPlayer mp = new MediaPlayer(media);
      mp.play();
      
      Stage tela = (Stage) btnConfirmar.getScene().getWindow();
      Parent parent = FXMLLoader.load(getClass().getResource("TelaDeEleitor.fxml"));
      Scene cenario = new Scene(parent);
      tela.setScene(cenario);
      tela.show();
    }
  }

  private void setCedula(Cedula cedula) {
    this.cedula = cedula;
  }

  @Override
  public void start(Stage tela) throws Exception {
    tela.setTitle("Urna Eletr�ncia RMI");
    URL urlEleitor = ClienteVotacaoFx.class.getResource("TelaDeEleitor.fxml");
    Pane pnlEleitor = (Pane) FXMLLoader.load(urlEleitor);
    tela.setScene(new Scene(pnlEleitor));
    tela.show();
  }
}